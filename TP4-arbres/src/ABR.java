import java.util.Objects;

public class ABR{
    private int val;
    private ABR filsG;
    private ABR filsD;
    //fisG == null <=> filsD == null

    //on supposera en prérequis sous-entendu que this est un ABR dans toutes les méthodes (sauf mentionné explicitement,
    //pour  la méthode verifie)
    ///////////////////////////////////////////////////////
    ////// méthodes fournies
    ///////////////////////////////////////////////////////

    public ABR(){
        filsG = null;
        filsD=  null;
    }

    public ABR(int x, ABR g, ABR d){
        val = x;
        filsG = g;
        filsD=  d;
    }

    public int getVal(){
        //sur arbre non vide
        if(estVide())
            throw new RuntimeException("getVal appelée sur ABR vide");
        return val;
    }


    public boolean estVide(){
        return filsG==null;
    }

    public boolean egal(ABR a){
        throw new RuntimeException("méthode non implémentée");

    }


    public String toStringV2aux(String s){
        //pre : aucun
        //resultat : retourne une chaine de caracteres permettant d'afficher this dans un terminal (avec l'indentation du dessin precedent, et en ajoutant s au debut de chaque ligne ecrite) et passe a la ligne

        if( estVide ())
            return s+"()\n";
        else
            return filsD.toStringV2aux (s + "     ") + s + getVal() + "\n"+ filsG.toStringV2aux (s + "     ");
    }

    public String toString(){
        return toStringV2aux("");
    }





    ///////////////////////////////////////////////////////
    ////// méthodes demandées dans le TP
    ///////////////////////////////////////////////////////

    public boolean recherche(int x){
        throw new RuntimeException("méthode non implémentée");
    }
    public void insert(int x){
        throw new RuntimeException("méthode non implémentée");
    }

    public int max(){
        //retourne l'entier max de this (et -infini si vide)
        throw new RuntimeException("méthode non implémentée");
    }


    public void suppr(int x){
        //supprime x de this, et ne fait rien si x n'est pas présent
        throw new RuntimeException("méthode non implémentée");
    }



    public boolean verifieNaive(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR
        throw new RuntimeException("méthode non implémentée");
    }
    public boolean verifieV1(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        //appel à verifABRV1
        throw new RuntimeException("méthode non implémentée");
    }

    public boolean verifABRV1(int m, int M){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR
        throw new RuntimeException("méthode non implémentée");
    }

    public boolean verifieV2(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        //appel à verifABRV2
        throw new RuntimeException("méthode non implémentée");
    }

    public int[] verifABRV2(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        throw new RuntimeException("méthode non implémentée");
    }




    ///////////////////////////////////////////////////////
    ////// méthodes utiles seulement pour les tests
    ///////////////////////////////////////////////////////



    public ABR getFilsG() {
        return filsG;
    }

    public ABR getFilsD() {
        return filsD;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public void setFilsG(ABR filsG) {
        this.filsG = filsG;
    }

    public void setFilsD(ABR filsD) {
        this.filsD = filsD;
    }

    public static void main(String[] args){



    }

}
